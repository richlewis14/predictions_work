module ApplicationHelper

  def login_status
    if user_signed_in?
      link_to("Logout", destroy_user_session_path, :confirm => "Are you sure you want to logout", :method => :delete)
    else
      link_to("Login", new_user_session_path)
    end
  end

  def resource_name
    :user
  end
 
  def resource
    @resource ||= User.new
  end
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  #enables omniauth in shared links
  def resource_class
    devise_mapping.to
  end

end
