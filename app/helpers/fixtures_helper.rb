module FixturesHelper
  def date_format(date)
    date.to_date.strftime("#{date.to_date.day.ordinalize} %b %Y")
  end

end
