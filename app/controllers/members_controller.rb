class MembersController < ApplicationController
  before_filter :authenticate_user!
  def index
  	three_days_from_today = Date.today + 3
    @fixtures = Fixture.where("fixture_date <= ?", three_days_from_today)
    @fixture_date = @fixtures.group_by {|fd| fd.fixture_date}

  end

end
