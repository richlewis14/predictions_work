class MiniLeaguesController < ApplicationController

  def index
    @minileague = MiniLeague.all

  end

  def new
    @minileague = MiniLeague.new

  end

  def create
    @minileague = MiniLeague.new(params[:mini_league])
      if @minileague.save
        redirect_to root_path, :notice => 'Mini League Succcessfully Created'
      else
        render :new
      end

  end
end
