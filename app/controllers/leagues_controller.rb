class LeaguesController < ApplicationController
  def new
    @league = League.new
  end
 
  def create
   @league = League.new(params[:league])
    if @league.save
      redirect_to root_path
    else
      render :new
    end
  end
end
