class EnrollmentsController < ApplicationController
  def index
  @enrollment = Enrollment.all
  end

  def new
    @enrollment = Enrollment.new

  end

  def create
    @enrollment = Enrollment.new(params[:enrollment])
      if @enrollment.save
      redirect_to root_path, :notice => 'You have joined the league'
      else
      # error, automanaged for custom model validator if comes from a non valid league key, show the form again
      render :new
    end

  end
end
