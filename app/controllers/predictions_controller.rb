class PredictionsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @predictions = current_user.predictions if current_user.predictions
  end

	def new
     @prediction = Prediction.new
     
	end

	def create
    begin
      params[:predictions].each do |prediction|
        Prediction.new(prediction).save!
      end
      redirect_to root_path, :notice => 'Predictions Submitted Successfully'
    rescue
      render 'new'
    end
  end


end
