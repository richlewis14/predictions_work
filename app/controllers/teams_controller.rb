class TeamsController < ApplicationController

  def index
    @team = Team.all

  end

  def new
    @team = current_user.teams.new

  end

  def create
    @team = current_user.teams.new(params[:team])
    if @team.save
      redirect_to root_path, :notice => 'Team Succcessfully Created'
    else
      render :new
    end

  end
end
