class Prediction < ActiveRecord::Base
   attr_accessible :home_team, :away_team, :home_score, :away_score, :fixture_date, :fixture_id, :user_id

   has_one :fixture

   validate :fixture_id, :uniqueness => { :scope => :user_id, :message => "only one prediction per game is allowed, for each user" }

end
