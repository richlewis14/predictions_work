class Team < ActiveRecord::Base
   attr_accessible :team_name, :user_id, :key

   belongs_to :user
   has_many :mini_leagues

end
