class Enrollment < ActiveRecord::Base
   attr_accessible :team_id, :mini_league_id

   belongs_to :team
   belongs_to :mini_league

   validate :valid_key?

    protected
    def valid_key?
      errors.add(:base, "Invalid key") unless team.key == mini_league.league_key
    end
end
