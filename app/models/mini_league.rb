class MiniLeague < ActiveRecord::Base
  attr_accessible :league_name, :league_key
  
  has_many :teams, through: :enrollments
end
