require 'spec_helper'
describe PredictionsController do
login_user

  describe "visit Prediction ( Signed in User Only)" do
    
    it "should display Prediction page" do
      get :index
      response.should be_success
      response.should render_template(:index)
    end
  end
end
