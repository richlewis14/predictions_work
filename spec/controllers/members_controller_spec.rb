require 'spec_helper'
describe MembersController do
login_user

  describe "visit Member Home Page ( Signed in User Only)" do
    
    it "should display Member Home Page" do
      get :index
      response.should be_success
      response.should render_template(:index)
    end
  end
end
