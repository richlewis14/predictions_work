class AddFixtureIdToPrediction < ActiveRecord::Migration
  def change
    add_column :predictions, :fixture_id, :integer
  end

  def down
    remove_column :predictions, :fixture_id
  end
end
