class ChangeFixtureDateFromStringToDate < ActiveRecord::Migration
  def up
    rename_column :fixtures, :fixture_date, :fixture_date_as_string
    add_column :fixtures, :fixture_date, :date
    remove_column :fixtures, :fixture_date_as_string
  end

  def down
    rename_column :fixtures, :fixture_date, :fixture_date_as_date
    add_column :fixtures, :fixture_date, :string
    remove_column :fixtures, :fixture_date_as_date
  end
end