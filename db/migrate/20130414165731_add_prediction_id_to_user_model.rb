class AddPredictionIdToUserModel < ActiveRecord::Migration
  def change
    add_column :users, :prediction_id, :integer
  end

  def down
  remove_column :users, :prediction_id
  end
end
