class AddNameToUsersModel < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :provider, :string
    add_column :users, :uid, :string
  end

  def down
    remove_column :users, :name
    remove_column :users, :provider, :string
    remove_column :users, :uid, :string

  end
end
