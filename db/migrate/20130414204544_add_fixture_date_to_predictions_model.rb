class AddFixtureDateToPredictionsModel < ActiveRecord::Migration
  def change
    add_column :predictions, :fixture_date, :text
  end

  def down
    remove_column :predictions, :fixture_date
  end
end
