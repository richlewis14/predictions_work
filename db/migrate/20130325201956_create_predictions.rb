class CreatePredictions < ActiveRecord::Migration
  def change
    create_table :predictions do |t|
    	t.text :home_team
    	t.text :away_team
    	t.integer :home_score
    	t.integer :away_score
    	t.integer :user_id

      t.timestamps
    end
  end
end
