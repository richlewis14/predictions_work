class AddMinileagueidToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :mini_league_id, :integer
  end

  def down
    remove_column :teams, :mini_league_id
  end
end
