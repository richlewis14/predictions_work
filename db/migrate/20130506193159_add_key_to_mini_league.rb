class AddKeyToMiniLeague < ActiveRecord::Migration
  def change
    add_column :mini_leagues, :league_key, :integer
  end

  def down
    remove_column :mini_leagues, :league_key

  end
end
