class CreateFixtures < ActiveRecord::Migration
  def change
    create_table :fixtures do |t|
    	t.string :home_team
    	t.string :away_team
    	t.date :fixture_date
    	t.datetime :kickoff_time

      t.timestamps
    end
  end
end
