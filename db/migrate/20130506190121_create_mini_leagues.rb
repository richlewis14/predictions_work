class CreateMiniLeagues < ActiveRecord::Migration
  def change
    create_table :mini_leagues do |t|
      t.string :league_name
      t.integer :user_id
      t.integer :team_id
      t.integer :invitation_id

      t.timestamps
    end
  end
end
