class AdKeyToTeams < ActiveRecord::Migration
  def up
    add_column :teams, :key, :integer
  end

  def down
    remove_column :teams, :key
  end
end
