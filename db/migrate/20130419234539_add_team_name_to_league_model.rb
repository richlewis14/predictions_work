class AddTeamNameToLeagueModel < ActiveRecord::Migration
  def change
    add_column :leagues, :team_name, :string
  end

  def down
    remove_column :leagues, :team_name
  end
end
