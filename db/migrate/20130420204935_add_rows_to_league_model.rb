class AddRowsToLeagueModel < ActiveRecord::Migration
  def change
    add_column :leagues, :played, :integer
    add_column :leagues, :goal_difference, :integer
    add_column :leagues, :points, :integer
  end

  def down
    remove_column :leagues, :played
    remove_column :leagues, :goal_difference
    remove_column :leagues, :points
  end
end
