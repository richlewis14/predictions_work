class AssPredictionIdToFixtures < ActiveRecord::Migration
  def up
  	add_column :fixtures, :prediction_id, :integer
  end

  def down
  	remove_column :fixtures, :prediction_id
  end
end
