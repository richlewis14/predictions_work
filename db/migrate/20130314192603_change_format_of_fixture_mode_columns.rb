class ChangeFormatOfFixtureModeColumns < ActiveRecord::Migration
  def up
  	change_column :fixtures, :fixture_date, :text
  	change_column :fixtures, :kickoff_time, :text
  end

  def down
  end
end
