#Run rake tasks via cron Heroku (Uses Scheduler)
desc "This task is called by the Heroku scheduler add-on (update league table)"
task :league => :environment do
  LeagueTable::LeagueTable.new.perform
end

desc "This task is called by the Heroku scheduler add-on (update fixture list)"
task :fixtures => :environment do
  MatchFixtures::MatchFixtures.new.perform
end

