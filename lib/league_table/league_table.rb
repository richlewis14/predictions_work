module LeagueTable
  class LeagueTable
    include LeagueGrabber::GetTable

    def perform
      update_league
      get_league_table
    end

    def update_league #rake task method
      League.destroy_all
     end
  end
end