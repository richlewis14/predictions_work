require 'open-uri'
require 'nokogiri'

module MatchGrabber::GetMatch

FIXTURE_URL = "http://www.bbc.co.uk/sport/football/premier-league/fixtures"

def get_fixtures # Get me all Home and away Teams
  doc = Nokogiri::HTML(open(FIXTURE_URL))
  days = doc.css('#fixtures-data h2').each do |h2_tag|
    date = Date.parse(h2_tag.text.strip).to_date
    matches = h2_tag.xpath('following-sibling::*[1]').css('tr.preview')
      matches.each do |match|
        home_team = match.css('.team-home').text.strip
        away_team = match.css('.team-away').text.strip
        kick_off = match.css('td.kickoff').text.strip
         Fixture.create!(home_team: home_team, away_team: away_team, fixture_date: date, kickoff_time: kick_off)
    end
  end
end


end